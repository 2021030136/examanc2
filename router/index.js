import express from 'express';
export const router = express.Router();
export default {router};

// Ruta por omisión
router.get('/', (req, res) => {
    res.render('index', {titulo: "Mis prácticas JS", nombre: "oscar Solis"});
});

// Ruta para mostrar el formulario de pago
router.get('/index', (req, res) => {
    res.render('index');
});

// Ruta para mostrar el recibo
router.get('/Recibo', (req, res) => {
    // Obtén los parámetros de la consulta
    const params = {
        num_docente: req.query.num_docente,
        nombre: req.query.nombre,
        domicilio: req.query.domicilio,
        nivel: req.query.nivel,
        pago_base: req.query.pago_base,
        horas_impartidas: req.query.horas_impartidas,
        cantidad_hijos: req.query.cantidad_hijos,
    };
    // Renderiza la vista de recibo con los parámetros
    res.render('Recibo', params); // Asumiendo que la vista "Recibo" está en la carpeta de vistas principal
});

// Ruta para procesar el formulario de pago y mostrar el recibo
router.post('/Recibo', (req, res) => {
    // Obtén los datos del formulario
    const { num_docente, nombre, domicilio, nivel, pago_base, horas_impartidas, cantidad_hijos } = req.body;
    // Renderiza la vista de recibo con los datos del formulario
    res.render('Recibo', { num_docente, nombre, domicilio, nivel, pago_base, horas_impartidas, cantidad_hijos }); // Asumiendo que la vista "Recibo" está en la carpeta de vistas principal
});