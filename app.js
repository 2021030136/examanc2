import http from 'http';
import path from 'path';
import express from 'express';
import bodyParser from 'body-parser';
import { fileURLToPath } from 'url';
import {router} from './router/index.js';

const puerto = 80;

const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);
// Crear una instancia de Express
const app = express();

// Configurar el motor de vistas
app.set("view engine", "ejs");

// Middleware para analizar datos JSON y datos de formulario
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

// Establecer el directorio de archivos estáticos
app.use(express.static(path.join(__dirname, '/public')));

// Usar las rutas definidas en el archivo router/index.js
app.use(router);

// Crear servidor HTTP y escuchar en el puerto especificado
const server = http.createServer(app);

server.listen(puerto, () => {
    console.log("Iniciando el servidor en el puerto " + puerto);
});
